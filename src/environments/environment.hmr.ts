export const environment = {
  production: false,
  apiUri: 'http://' + window.location.hostname + ':3000/api',
  hmr: true
};
